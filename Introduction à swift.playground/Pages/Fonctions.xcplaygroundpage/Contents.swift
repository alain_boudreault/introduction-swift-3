//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation; import UIKit
//: # Les fonctions
//: * 1  – Fonction sans paramètre
//: * 2  – Fonction avec des paramètres nommés
//: * .1 – Fonction avec des paramètres nommés externes
//: * .2 – Fonction avec des paramètres non nommés
//: * 3  - Fonction qui retourne une valeur
//: * 4  - Paramètres avec une valeur par défaut
//: * 5  - Nombre variable de paramètres
//: * 6  - Valeur de retour optionnelle
//: * 7  - Paramètres passés en référence
//: * 8  – Fonction qui retourne plusieurs valeurs (tuplet)
//: * 9  – Recevoir une fonction en paramètre
//: * 10 - Retourner une fonction
//: * 11 - Paramètres de type générique
//:
//:---
//: ## 1 – Fonction sans paramètre
//: Déclaration: func nomFonction() { code }
func besoinDeRien(){
    print("--> ligne \(#line): fn0 - Je ne recois rien de rien!")
}
besoinDeRien()

//:---
//: ## 2 – Fonction avec des paramètres nommés
//: Déclaration: func nomFonction(nomParamètre:TypeDuParamètre, ...) { code }
func besoinDeParamètres(param1:Int, param2:String, param3:Any){
    // Note: Les paramètres sont en lecture seulement.
    print("--> ligne \(#line): fn2  - param1: \(param1), param2: \(param2), param3: \(param3)")
} // fn2

besoinDeParamètres(param1: 3, param2: "1415", param3: 3.1415)

//: ## 2.1 – Fonction avec des paramètres nommés externes
//: Déclaration: func nomFonction(nomParamètre:TypeDuParamètre, ...) { code }
func mesParamètresOntDesNomsExternes(age param1:Int, nom param2:String, image param3:Any){
    print("--> ligne \(#line): fn2a - param1: \(param1), param2: \(param2), param3: \(param3)")
} // fn2a

mesParamètresOntDesNomsExternes(age: 99, nom: "Yo", image: UIImage())

//: ## 2.2 – Fonction avec des paramètres non nommés
//: Déclaration: func nomFonction(_ nomParamètre:TypeDuParamètre, ...) { code }
func mesParamètresNeSontPasNommés(_ param1:Int, _ param2:String, _ param3:Any){
    print("--> ligne \(#line): fn2b - param1: \(param1), param2: \(param2), param3: \(param3)")
} // fn2b

mesParamètresNeSontPasNommés(3, "1415", 3.1415)

//:---
//:  ## 3 - Fonction qui retourne une valeur
func jeRetourneLaSommeDeAetB(deA:Int, etDeB:Int) -> Int {
    return deA + etDeB
} // somme

let résultat = jeRetourneLaSommeDeAetB(deA: 1, etDeB: 2)
print("--> ligne \(#line): \(résultat)")

//:---
//:  ## 4 - Paramètres avec une valeur par défaut
func fnAvecParamètresParDéfaut(param1:Int, param2:Int = 0, param3:String = "n/a"){
    print("--> ligne \(#line): fn4a - param1: \(param1), param2: \(param2), param3: \(param3)")
}

func fnAvecTousLesParamètresParDéfaut(_ param1:Int = 0, _ param2:Int = 0, _ param3:String = "n/a"){
    print("--> ligne \(#line): fn4b - param1: \(param1), param2: \(param2), param3: \(param3)")
}

fnAvecParamètresParDéfaut(param1: 12, param3: "Yo!")
fnAvecTousLesParamètresParDéfaut(12, 23)

//:---
//: ## 5 - Nombre variable de paramètres
func additionner(_ desNombres:Int ...) -> Int {
    var total = 0
    for i in desNombres {
        total += i
    } // for
    return total
} // additionner

print("--> ligne \(#line): additionner: \(additionner(1,2,3,4,5))")

//:---
//: ## 6 - Valeur de retour optionnelle
func sommation(_ nombres:Int ...) -> Int? {
    // Si aucun paramètre, alors retourner 'nil'
    if nombres.count == 0 { return nil }
    
    // Boucler sur les paramètres et les additionner...
    var total = 0

    for nombre in nombres {
        total += nombre
    } // for nombre

    // Retourner le résultat
    return total
} // somme

// Utilisation
if let resultat = sommation(1,2,3,4,5) {
    print("--> ligne \(#line): La somme de tous ces nombres donne: \(resultat)")
} else { print("--> ligne \(#line): Erreur: Aucun nombre à additionner...") }

// Utilisation
if let totalJoueurs = sommation(0,1,0) {
    let pluriel = totalJoueurs>1 ? "s" : ""
    print("--> ligne \(#line): Il y a \(totalJoueurs) joueur\(pluriel) sur le terrain")
}

//:---
//: ## 7 - Paramètres passés en référence
func incrémenterPaire( p1:inout Int,  p2:inout Int){
    // Il sera alors possible de modifier le contenu original des paramètres.
    p1 += 1; p2 += 1
}

var i7 = 2, i7b = 1414
incrémenterPaire(p1: &i7, p2: &i7b)
print ("--> ligne \(#line): i7 = \(i7), i7b = \(i7b)")

//:---
//: ## 8 – Fonction qui retourne plusieurs valeurs (tuplet)
// Fonction qui retour plusieurs valeurs en utilisant un n-uplet (anglais:tuple)
func fn8(_ p1:Int, _ p2:Int) -> (age:Int, force:Int) {
    return (p1 + 1, p2 + 1)
}

// Utilisation
let x = fn8(1,2)
x.age    // == 2
x.force  // == 3


//:---
//: ## 9 – Recevoir une fonction en paramètre
// Étant donné les trois fonctions suivantes:
func additionner (a:Int, b:Int) -> Int {return a + b}
func soustraire (a:Int, b:Int) -> Int {return a - b}
func multiplier (a:Int, b:Int) -> Int {return a * b}

// alors
func operationSur2Nombres(fonction: (Int,Int) -> Int, _ nb1:Int, _ nb2:Int) -> Int{
    return fonction(nb1,nb2)
}

// Exemple d'utilisation:
operationSur2Nombres(fonction: additionner, 30000, 1415)

//:---
//: ## 10 - Retourner une fonction
func choisirUneFonction(votreChoix:Int) -> (Int,Int) -> Int{
    // -> (Int,Int) -> Int = retourne une fonction qui nécéssite deux params de type Int et qui retourne une valeur de type Int.
    switch votreChoix {
        case 2:  return soustraire
        case 3:  return multiplier
        default: return additionner
    }
} // choisirUneFonction

// Ou bien avec un type enum
enum Operateurs {
    case addition       // Les constantes = 0..n
    case soustraction
    case multiplication
}

func choisirUneFonction(opérateur:Operateurs) -> (Int,Int) -> Int{
    switch opérateur {
        case .addition:       return additionner
        case .soustraction:   return soustraire
        case .multiplication: return multiplier
    } // switch
} // choisirUneFonction

// Exemple d'utilisation:
operationSur2Nombres(fonction: choisirUneFonction(votreChoix: 2), 12, 14)
operationSur2Nombres(fonction: choisirUneFonction(opérateur: .addition), 12, 14)

//:---
//: ## 11 - Paramètres de type générique
//: exemple 1
func échanger<T>(_ param1:inout T, _ param2:inout T){
    let temp = param1
    param1 = param2
    param2 = temp
}

//: Utilisation avec Int
var nb1=2; var nb2=5
print("--> ligne \(#line): nb1 = \(nb1),nb2 = \(nb2)")
échanger(&nb1, &nb2)
print("--> ligne \(#line): nb1 = \(nb1),nb2 = \(nb2)")

//: Utilisation avec String
var chaine1="Bob"; var chaine2="Binette"
print("--> ligne \(#line): chaine1 = \(chaine1), chaine2 = \(chaine2)")
échanger(&chaine1, &chaine2)
print("--> ligne \(#line): chaine1 = \(chaine1), chaine2 = \(chaine2)")

//: Exemple 2 - conforme au protocole 'Comparable'
func minimum<T: Comparable >(_ nb1:T, _ nb2:T) ->T {
    return nb1 < nb2 ?nb1:nb2
}
// Utilisation
// Le type du paramètre doit être conforme au protocole 'Comparable'
// Voir le fichier.h des types Int, Float et String.
minimum(9,4)
minimum(9.7,4.2)
minimum("petit","grand")
// minimum(UIColor.red, UIColor.green)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: [Référence - Fonctions](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Functions.html)
//: [Référence - Génériques](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Generics.html)

//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault
