//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import UIKit
//: # Divers

let carré = UIView(frame: CGRect(x: 50, y: 0, width: 50, height: 50))
carré.backgroundColor = UIColor.red

//carré.transform = CGAffineTransform(rotationAngle: 15.0 * (3.1415 / 180.0));

/*:

## Literal Expression

A literal expression consists of either an ordinary literal (such as a string or a number), an array or dictionary literal, a playground literal, or one of the following special literals:

Literal     Type    Value
 
\#file       String  The name of the file in which it appears.
 
\#line       Int     The line number on which it appears.
 
\#column     Int     The column number in which it begins.
 
\#function   String  The name of the declaration in which it appears.
 

*/

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault
