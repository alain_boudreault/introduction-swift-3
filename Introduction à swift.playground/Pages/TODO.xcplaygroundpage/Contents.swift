//: [Previous](@previous)

import Foundation

/*

 * Expliquer les opérateurs === et !==  // Identity Operators
 * Expliquer; Lazy Stored Properties
 * Expliquer; Read-Only Computed Properties
 * Expliquer; Property Observers (willSet, didSet)
 * Expliquer la notion de variable 'static' en swift
 * Expliquer; Querying and Setting Type Properties
 * Fournir plus d'exemples de setters et getters
 
*/

class UneClasse {
    private var _i:Int
    internal var i:Int {
        get { return _i }
        set(valeur /* newValue */) { _i = valeur >= 0 ? valeur : 0 }
    }
    
    init(_ i:Int) {
        _i = i
    }
} // class UneClasse

let uneClasse = UneClasse(99)
uneClasse.i
uneClasse.i = -24
uneClasse.i



//: [Next](@next)
