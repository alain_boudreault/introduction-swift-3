//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
//: # Fonctions anonymes - closure
import Foundation
/*:
 
 Declaring a closure
 The general syntax for declaring closures is:
 
 { (parameters) -> return type in
 statements
 }
 
 If the closure does not return any value you can omit the arrow (->) and the return type.
 
 { (parameters) in
 statements
 }
 
 Closures can use variable and inout parameters but cannot assign default values to them. Also closure parameters cannot have external names.
 
 */

// a closure that has no parameters and return a String
var hello: () -> (String) = {
    return "Hello!"
}

hello() // Hello!

// a closure that take one Int and return an Int
var leDoubleDe: (Int) -> Int = { x in
    return 2 * x
}

leDoubleDe(2) // 4

// you can pass closures in your code, for example to other variables
var alsoDouble = leDoubleDe
alsoDouble(3) // 6

// Avec plusieurs paramètres
var laSommeDe: (Int, Float) -> Float = {unInt, unFloat in
    return Float(unInt) + unFloat
}

laSommeDe(3, 0.141592)

// Avec des paramètres non nommés
var laSommeDeIntEtFloat: (Int, Float) -> Float = {
    return Float($0) + $1
}

laSommeDeIntEtFloat(3, 0.141592)


let digitNames = [
    0: "Zéro", 1: "Un", 2: "Deux",   3: "Trois", 4: "Quatre",
    5: "Cinq", 6: "Six", 7: "Sept", 8: "Huit", 9: "Neuf"
]
let numbers = [16, 58, 510]
//The code above creates a dictionary of mappings between the integer digits and English-language versions of their names. It also defines an array of integers, ready to be converted into strings.

//You can now use the numbers array to create an array of String values, by passing a closure expression to the array’s map(_:) method as a trailing closure:

let strings = numbers.map {
    (number:Int) -> String in
    var nombre = number  // parce que 'number' est une constante.
    var output = ""
    repeat {
        output = digitNames[nombre % 10]! + output
        nombre /= 10
    } while nombre > 0
    return output
}
// strings is inferred to be of type [String]
// its value is ["OneSix", "FiveEight", "FiveOneZero"]


// Convertir en caractères minuscules toutes les chaines du tableau 'amis'
let amis = ["Paul", "Fred", "Lise", "Yanick"]
let lowercaseNames = amis.map {
    (unNom) -> String in unNom.lowercased() }
// Ou bien, forme courte:
let lowercaseNames2 = amis.map { $0.lowercased() }
// $0 = premier paramètre reçu.

let letterCounts = amis.map { $0.characters.count }
// 'letterCounts' == [6, 6, 3, 4]

let desNombres = [92,15,3,14]
desNombres.sorted()  // ordre croissant
desNombres.sorted(by: { x, y in
    return y < x     // order décroissant
})

// **********************************
// Fonction qui recoit une 'closure'

func uneFonctionQuiRecoitUneClosure(closure: () -> Void) {
    print("Je suis en marche ...")
    closure()
}

// Utilisation:
uneFonctionQuiRecoitUneClosure(){
    print("\(#line): Je suis une fonction")
    print("Qui recoit une 'closure'")
}

// Autre syntaxe:
uneFonctionQuiRecoitUneClosure(closure: {
    for compteur in 0...2 {
        print("Je suis \(compteur)")
    }
})

//: [Closures sur stackoverflow](http://stackoverflow.com/documentation/swift/262/closures#t=20161106235815174476)

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Closures.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault
