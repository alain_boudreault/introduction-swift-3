//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation

// Protocole, définition: fonctionalités modulaires qui peuvent-être adoptées par une ou plusieurs classes.

//: 1 - Un contrat qui défini des 'exigences (requirements)' pour l'adhérent
//: Le contrat peut-inclure l'obligation d'implémenter des:
//: #Propriétés
//: #Méthodes
//: #Indices (subscript), [2...5]

//2: L'adhérent déclare son adoption au protocole et doit s'y conformer.

//3: Un protocole peut hériter d'autres protocoles.

//4: Un protocole peut-être étendu, ajout de fonctionnalités.

//:  On déclare un protocole en utilisant la synthaxe suivante:
//:  protocol NomDuNouveauProtocole { ... }

//: Note: un protocole définit des exigences mais ne les implémente pas.  Il est de la responsabilité de l'adhérent de les implémenter dans les classes qui adapteront le ou les protocoles.

protocol SeDéplace{
    var vitesse:Float {get}  // L'adhérent pourra redéclarer la constante en variable même l'il n'y a pas de 'setter' ici.
    var autonomieSansRepos:Float {get set}
    
    func déplacerDe(km: Float) -> Float
}

protocol SeDéplaceEnMarchant: SeDéplace{
    // Propriété de protocole doit avoir getter/setter
    // Ne peut pas être initialisée à la déclaration; var i = 0
    var nbPattes:Int {get}
}

protocol SeDéplaceEnCourant: SeDéplace{
    // Propriété de protocole doit avoir getter et/ou setter
    // Ne peut pas être initialisée à la déclaration; par exemple, var i = 0
    var vitesseMoyenne:Float {get set}
}

protocol PeutMordre{
    var probabilité:Float {get set}
    var dommage:Int {get set}
}

class Pitou: SeDéplaceEnMarchant, PeutMordre, CustomStringConvertible {

    // Le protocole 'CustomStringConvertible' permet, via la prop 'description' de personnaliser la chaine retournée lorsque l'on affiche la valeur d'une instance de la classe.  Par exemple, print(unPitou as Pitou)

    // Implémentation des prop. de PeutMordre
    internal var dommage: Int = 0
    internal var probabilité: Float = 10.0

    internal var nbPattes: Int = 4

    // Implémentation des prop. de SeDéplaceEnMarchant
    var vitesse: Float = 3.5
    var autonomieSansRepos: Float = 15.0
    
    // Implémentation des prop. locales
    private var doitSeReposer = false
    private var distanceParcourue:Float = 0.0

    // Implémentation des métodes de SeDéplace
    internal func déplacerDe(km: Float) -> Float {
        distanceParcourue += km
        doitSeReposer = distanceParcourue >= autonomieSansRepos ? true : false
        return doitSeReposer ? 0 : km
    }

    var description: String { return "Je suis un Pitou à \(nbPattes) patte(s)\nJe peux me déplacer à \(vitesse) km/h sur une distance de \(autonomieSansRepos) km sans me reposer.\nIl y a une probabilité de \(probabilité)% que je puisse vous mordre." }
}

let unPitou = Pitou()
unPitou.dommage = 99
print(unPitou)
unPitou.déplacerDe(km: 14.5)
unPitou.déplacerDe(km: 5)



//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.02.24 par Alain Boudreault

