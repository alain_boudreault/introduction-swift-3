//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

import Foundation

/*: Implémentation des protocoles:

 - Sequence: permet de boucler sur l'instance

 - IteratorProtocol: permet d'avoir accès à la prochaine valeur de la liste.  
 
    Dans une boucle, une valeur 'nil' met fin à l'itération.  Pour être conforme au protocole, il faut implémenter la  méthode next() -> T?

 - Normalement, ces deux protocoles sont implémentés de pair.
*/

struct Countdown: /*Sequence ,*/ IteratorProtocol  {
    var count: Int
    
    mutating func next() -> Int? {
        if count == 0 {
            return nil
        } else {
            defer { count -= 1 }
            return count
        }
    }
}

//: Exemple d'utilisation:

var threeToGo = Countdown(count: 3)

print (threeToGo.next())
print (threeToGo.next())
print(threeToGo)

/*
 // Ne fonctionne que si conforme au protocole : Sequence
 for i in threeToGo {
    print(i)
 }
 */

//: Un 'séquenceur' de nombres pairs

struct Pair: Sequence, IteratorProtocol {
    var count: Int
    var maxItération: Int
    // mutating: Par défaut, les propriétés d'une structure sont en lecture seule.  'mutating' donne accès aux prop. en mode écriture.
    mutating func next() -> Int? {
        guard maxItération > 0 else {return nil}
        guard count % 2 == 0 else {return nil} // s'assurer de recevoir un nombre pair.
            defer  { count += 2 } // defer = incrémenter après le retour de la fonction, histoire d'avoir le premier nombre de la séquence.
            maxItération -= 1
            return count
    } // next()
}

var even = Pair(count: 0, maxItération: 100)
for _ in even { // arrêt de l'itération à 'nil'
    if let val = even.next(){
        print(val)
    }
}

//=============================================
// Suite de Fibonacci
// https://fr.wikipedia.org/wiki/Suite_de_Fibonacci
struct Fibonacci: Sequence, IteratorProtocol {
        var _last: Int = 1
        var _laster: Int = 1
        var _count: Int = 0
        var _result: Int = 0
        
        mutating func next() -> Int? {
            _count += 1
            guard _count != 1 else {
                return _laster
            }
            guard _count != 2 else {
                return _last
            }
            _result = _last + _laster
            _laster = _last
            _last = _result
            return _result
       } // next()
}

print("\nVoici un extrait de la suite de Fibonacci:")
var fibo = Fibonacci()
for _ in 0..<10 {
    if let val = fibo.next(){
        print(val)
    }
}

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.03.14 par Alain Boudreault
