//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
import UIKit

//: # Date et DateFormatter
//: ![tableau des formats](dateFormat.png "tableau des formats de date")

//: # 1.Date

// Obtenir la date du jour
let currentDate = Date()
let currentDateComponents = Calendar.current.dateComponents([.hour, .minute, .second], from:currentDate)

let dateFormatter = DateFormatter()
// Voir dateFormat.png
dateFormatter.dateFormat = "yyyy-MM-dd HH:mm vvvv"
let stringFromDate = dateFormatter.string(from:currentDate)

// Créer une date à partir d'une chaine
let uneStringDate = "2017-02-05T23:59:59-05:00" // Format UCT
dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssxxxxx"
if let dateFromString = dateFormatter.date(from:uneStringDate) {
    dateFormatter.dateFormat = "'Le' yyyy-MM-dd HH:mm"
    let stringFromDate = dateFormatter.string(from:dateFromString)
    print ("stringFromDate: \(stringFromDate)")
}

// Créer une date à partie d'un DateComponents
var dateComponents = DateComponents()
dateComponents.year = 2017
dateComponents.month = 02
dateComponents.day = 13
dateComponents.hour = 23
dateComponents.minute = 59
dateComponents.second = 59
let date = Calendar.current.date(from :dateComponents)!


// Référence: http://nshipster.com/nsformatter/

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.02.16 par Alain Boudreault
