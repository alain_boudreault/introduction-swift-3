//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
//: # Structures et classes

//: # 0.Porté des déclarations:

/*:

The Five Access Levels of Swift 3
 
Swift 3 has five access levels that control from which source file or module you can access something. In order from most open to most restricted:

'**open**' -  you can access open classes and class members from any source file in the defining module or any module that imports that module. You can subclass an open class or override an open class member both within their defining module and any module that imports that module.

'**public**' -  allows the same access as open - any source file in any module - but has more restrictive subclassing and overriding. You can only subclass a public class within the same module. A public class member can only be overriden by subclasses in the same module. This is important if you are writing a framework. If you want a user of that framework to be able to subclass a class or override a method you must make it open.

'**internal**' -  allows use from any source file in the defining module but not from outside that module. This is generally the default access level.

'**fileprivate**' - allows use only within the defining source file.

'**private**' - allows use only from the enclosing declaration.

 Référence: http://useyourloaf.com/blog/swift-3-access-controls/
 
 */


//: # 1.Structures
struct StructurePersonnage {
    var nom         = "Non définie"
    var niveau      = 0
    var force       = 0.0
    var positionX   = 0.0
    var positionY   = 0.0
    var vies:Int    = 0
} // StructurePersonnage

var jeSuisDeTypePersonnage = StructurePersonnage()
print("--> ligne \(#line): jeSuisDeTypePersonnage = \(jeSuisDeTypePersonnage)")

var moiAussiJeSuisDeTypePersonnage = StructurePersonnage(nom: "Bozo le clown", niveau: 3, force: 55, positionX: 0, positionY: 0, vies: 3)

moiAussiJeSuisDeTypePersonnage.vies -= 1

print("--> ligne \(#line): moiAussiJeSuisDeTypePersonnage = \(moiAussiJeSuisDeTypePersonnage)")

//: ## 1.1.Structures passées par copie

func uneFonctionQuiRecoitUneStructure( uneStructure:  StructurePersonnage) {
    // La fonction va recevoir une copie, en lecture seule, de la structure.
    // uneStructure.nom = "Le nom a été modifé!"  // va produire une erreur!
    // Solution:
    var uneStructure = uneStructure
    uneStructure.nom = "'## Le nom a été modifé ##'"
    print("--> ligne \(#line): \(uneStructure)")
}   // uneFonctionQuiRecoitUneStructure

uneFonctionQuiRecoitUneStructure(uneStructure: moiAussiJeSuisDeTypePersonnage)
print("--> ligne \(#line): moiAussiJeSuisDeTypePersonnage = \(moiAussiJeSuisDeTypePersonnage)")

//: ---------------------------
//: # 2.Classes
// Définition d'une classe:

class ClassePersonnage {
    // Des propriétés
    var nom         = "Non définie"
    var niveau      = 0
    var force       = 0
    var positionX   = 0.0
    var positionY   = 0.0
    var vies:Int    = 0
    
    // Un constructeur
    init(unNom:String = "Non définie", force:Int = 0) {
        self.nom    = unNom
        self.force  = force
    }
    
    // Le destructeur
    deinit {
        // perform the deinitialization
        // Faire le ménage...
        print("L'instance de \(self.nom) a été détruite...")
    }
    // Des méthodes
    func description() ->String {
        return "Je suis le personnage \(nom) et j'ai une force de \(force)."
    } // description()
    
    func deplacer(vitesse:Int, posX:Float, posY:Float) {
        print("--> ligne \(#line): \(nom) se déplace vers (\(posX),\(posY)) à la vitesse \(vitesse).")
    } // deplacer()
    
} // ClassePersonnage

// Utilisation d'une classe
var personnage = ClassePersonnage()
personnage.nom = "La terreur des barbares"
print("--> ligne \(#line): \(personnage.description())")
personnage.deplacer(vitesse: 99, posX: 100, posY: 200)

var personnage02 = ClassePersonnage(unNom: "Bozo le clown")

//: ## 2.1.Classes passées par référence
func uneFonctionQuiRecoitUneClasse(uneClasse: ClassePersonnage) {
    // La fonction va recevoir une référence à la classe.
    uneClasse.nom = "'## Le nom a été modifé ##'"
} // uneFonctionQuiRecoitUneClasse

uneFonctionQuiRecoitUneClasse(uneClasse: personnage)
print("--> ligne \(#line): personnage = \(personnage.description())")


//: ## 2.2.Les extensions de classe
// Ajouter une méthode à la classe ClassePersonnage
extension ClassePersonnage {
    func retourAuCamp() -> String {
        return "'\(nom)' revient au camp."
    }
} // extension ClassePersonnage

let perso_03 = ClassePersonnage(unNom: "Le voyageur")

print("--> ligne \(#line): \(perso_03.retourAuCamp())")

// ---------------------------------
// Avec une classe du langage swift:
extension String {
    func servirARien()->String{
        return "Quelle perte de temps pour '\(self)'!"
    } // servirARien()
} // extension String

let ouf = ""
ouf.servirARien()
"allo".servirARien()

//: ## 2.3.Héritage
// Construire une classe qui 'étend' la classe 'ClassePersonnage'
class PersonnageComplexe:ClassePersonnage {
    func methodeQuiTue() ->String {
        return "\(nom) est prit d'une rage et anéantit tous ses adversaires... glup!"
    } // methodeQuiTue
} // PersonnageComplexe

let mechant = PersonnageComplexe(unNom: "Boris")
print("--> ligne \(#line): \(mechant.methodeQuiTue())")


//: ## 2.3.Surchager une méthode de la super classe.
class PersonnageComplexeAvecDescriptionAméliorée:PersonnageComplexe {
    override func description() -> String {
        // super.description()  // au besoin
        return "Je suis le personnage complexe \(nom), \nj'ai une force de \(force) et je sais tuer!"
    } // override
} // PersonnageComplexeAvecDescriptionAméliorée

var unPersoDangereux = PersonnageComplexeAvecDescriptionAméliorée(unNom: "GrrDeGrr", force: 99)

unPersoDangereux.description()

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ClassesAndStructures.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault
