//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
//: # Les chaines de caractères

let chaine:String   = ""
let tim             = "Étudiants et étudiantes de CSTJ.TIM"
let cours           = "Introduction à Xcode"
var résultat        = String(27.0/5)
résultat            = String(Float(résultat)! / 2)  // Voir 'Variables et optionnelles'

//: # 1.Concaténation
var msgBienvenue = tim + ", bienvenue au cours : " + cours
msgBienvenue += "!"

//: # 2.Interpolation de chaines
let anneeCours      = 2016
msgBienvenue        = "\(tim), bienvenue au cours : \(cours) de l'année \(anneeCours)"
let nbSemaines      = 15
let heuresSemaine   = 4
let msg             = "À \(heuresSemaine) heures de cours par semaine pendant \(nbSemaines) semaines, le cours a une durée de \(nbSemaines * heuresSemaine) heures."

//: # 3.Comparaison de chaines
let perso1 = "Fred " + "LeToff"
let perso2 = "Fred LeToff"

if perso1 == perso2 {
    print("--> ligne \(#line): \(perso1) et \(perso2) sont une seule et même personne")
} else {
    print("--> ligne \(#line): Il n'y définitivement aucun lien de parenté entre \(perso1) et \(perso2)")
}

//: ## 3.1.Test insensible à la casse
let perso3 = "Fred " + "Letoff"
let perso4 = "Fred LeToff"

if perso3.lowercased() == perso4.lowercased() {
    print("--> ligne \(#line): \(perso3) et \(perso4) sont une seule et même personne")
} else {
    print("--> ligne \(#line): Il n'y définitivement aucun lien de parenté entre \(perso3) et \(perso4)")
}

//: ## 3.2.Tester des chaines vides
let jeSuisUneChaineVide = ""
let moiAussi = String()
let pasMoiJeSuisNil:String

if jeSuisUneChaineVide.isEmpty && moiAussi.isEmpty  /* && pasMoiJeSuisNil.isEmpty */ {
    print("--> ligne \(#line): Il n'y a que du vide ici...")
}

// Ou bien
if jeSuisUneChaineVide == "" { print("--> ligne \(#line): chaine vide testée avec \"\"") }

//: ## 3.3.Obtenir la longueur de la chaine
let uneCitation = "Il fit de la sorte un assez long chemin..."
uneCitation.characters.count    // .characters retourne un tableau de car.

//: ## 3.3.Itérer sur les caractères d'une chaine
let unChien = "Chien 🐶"
for car in unChien.characters {
    print(car,  terminator:" ")
}
print()
//: # 4.Traitements sur les chaines

//: ##  4.1.Chercher une sous-chaine
let phrase = "Il était une fois, TIM le courageux!"
if phrase.contains("TIM")
{
    print("\n--> ligne \(#line): 'TIM' est dans la phrase.\n")
}

//: ## 4.2.Extraire une sous-chaine
let uneSousChaine = phrase[phrase.index(phrase.startIndex, offsetBy: 9)...phrase.index(phrase.startIndex, offsetBy: 9+7)]


//: ## 4.3.Rempacer une sous-chaine par une autre
let phraseModifiée = phrase.replacingOccurrences(of: "TIM", with: "Toto").replacingOccurrences(of: "courageux", with: "peureux")

//: ## 4.4.Créer une chaine à partir d'un fichier
if let filepath = Bundle.main.path(forResource: "unFichier", ofType: "txt") {
    do {
        let contents = try String(contentsOfFile: filepath)
    } catch {
        print("--> ligne \(#line): Erreur: Impossible de lire le contenu du fichier!")
    }
} else {
    print("--> ligne \(#line): Erreur: fichier non trouvé!")
}

//: ## 4.5.Créer une chaine à partir d'une URL
do {
      let contents = try String(contentsOf: URL(string: "http://tim.cstj.qc.ca")!)
    } catch {
        print("--> ligne \(#line): Erreur: Le contenu n'a pas été chargé!")
}

//: ## 4.6.Créer un tableau à partir d'une chaine
var uneChaine = "Saint-Jérôme, Berlin, Paris, New York, San Francisco"
var unTableau = uneChaine.components(separatedBy: ",")

//: # 5.NSString, + de fonctionnalités
//: [Doc Apple](https://developer.apple.com/reference/foundation/nsstring)
let motCache:NSString = "swilfkjnsljfswjftswiftswjkfswftfdf"
print("--> ligne \(#line): Le mot caché est '\(motCache.substring(with:NSMakeRange(17, 5)))'")

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/StringsAndCharacters.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.18 par Alain Boudreault
