/*:

# Introduction à swift 3

 Support de cours pour la section d'introduction au langage swift 3 du cours Production multimédia sur support.
 
 Il a été écrit par; Alain Boudreault, aka [ve2cuy](https://ve2cuy.wordpress.com)
, aka [puyansude](https://www.youtube.com/puyansude), aka [cours Xcode](http://prof-tim.cstj.qc.ca/cours/xcode/wp/), pour les étudiants et étudiantes de Techniques d'Intégration Multimédia du cégep de Saint-Jérôme.

## Table des matières

* [Page blanche](La%20page%20blanche)
* [Variables et optionnelles](Variables%20et%20optionnelles)
* [Chaines de caractères](Chaines%20de%20caracteres)
* [Opérateurs](Operateurs)
* [Tableaux](Tableaux)
* [Dictionnaires](Dictionnaires)
* [Structures de contrôle](Structures%20de%20controle)
* [Fonctions](Fonctions)
* [Fonctions anonymes et closure](Fonctions%20anonymes%20et%20closure)
* [Énumérations et optionSets](EnumEtOptionSet)
* [Structures et classes](Structures%20et%20classes)
* [Gestion des erreurs](Gestion%20des%20erreurs)
* [Protocoles](Protocoles)
* [Divers](Divers)

---
 ""
 
 Pour plus d'information, voir le document: [The Swift Programming Language (swift 3).](http://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/)
*/
//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault

