//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
//: # Les structures de contrôle
import UIKit
//: # 1.Les boucles

//: ## 1.1.For-In
for index in 1...5 {
    print("--> ligne \(#line): \(index) fois 2 donne \(index * 2)")
}

//: Si l'index n'est pas requis alors:
for _ in 1...80 {
    print("-", terminator:"")
} ; print()

//: Boucler sur les éléments d'un tableau
let noms = ["Tintin", "Milou", "Haddock"]
for nom in noms {
    print("--> ligne \(#line): Je suis \(nom)!")
} ; print()

//: Boucler sur les éléments d'un tableau avec index
for (index, nom) in noms.enumerated() {
    print("--> ligne \(#line): Item \(index): Je suis \(nom)!")
}  ; print()

//: Boucler sur les champs d'un dictionnaire
let personnage = ["nom": "Fred", "force": "99", "niveau": "débutant"]
for (clé, contenu) in personnage {
    print("--> ligne \(#line): Clé \(clé): \(contenu)!")
}  ; print()
//: # 2.While

//: ## 2.1.While
/*:
    Voici la forme générale d'une boucle while:

    while condition {
        statements
    }
 
    Note: La condition est évaluée avant d'entrer dans le bloc d'instructions.
*/
var fois=0
while fois < 10 {
    fois += 5
}

//: ## 2.2.Repeat-While
/*:
 Voici la forme générale d'une boucle repeat-while:
 
 repeat {
    statements
 } while condition

 Note: Le bloc d'instructions sera évalué une fois, peu importe 'condition'.
 */

var finDeLaPartie = false
var vies = 3
repeat {
    print("--> ligne \(#line): Début de la partie!")
    while vies > 0 {
        print("--> ligne \(#line): Il me reste \(vies) vie(s)")
        vies -= 1
    } // while vies
    finDeLaPartie = true
} while !finDeLaPartie
print()
//: # 3.Instructions conditionnelles

//: ## 3.1.If
let noteObtenue = 90
if noteObtenue <= 60 {
    print("--> ligne \(#line): Vous n'avez pas obtenu la note de passage.")
} else
    if noteObtenue >= 90 {
         print("--> ligne \(#line): Bravo, vous méritez une mention!")
    } else {
            print("--> ligne \(#line): Travail bien réussi.")
    }
print()
//: ## 3.2.Switch
/*:
 Voici la forme générale d'un switch:
 
    switch 'une valeur à considérer' {
        case     valeur 1:  'réponse à valeur 1'
        case     valeur 2,
                 valeur 3:  'réponse à valeur 2 ou 3'
        default: 'autrement, faire autre chose ici...'
    }
 
 Note: La clause **'default'** est obligatoire si tous les cas ne sont pas traités.
 */

vies = 2
switch vies {
    case 0:     print("--> ligne \(#line): Té mort!")
    case 1,2:   print("--> ligne \(#line): Attention, la fin est proche...!")
    case 3:     print("--> ligne \(#line): Quelle forme tu as!")
    default:    print("--> ligne \(#line): Tu as certainement triché!")
}

//: ## 3.2.1.Switch avec des étendus
//: Note: Exemple provenant du document 'swift 3' d'Apple.
let approximateCount = 62
let countedThings = "moons orbiting Saturn"
var naturalCount: String
switch approximateCount {
    case 0:             naturalCount = "no"
    case 1..<5:         naturalCount = "a few"
    case 5..<12:        naturalCount = "several"
    case 12..<100:      naturalCount = "dozens of"
    case 100..<1000:    naturalCount = "hundreds of"
    default:            naturalCount = "many"
}
print("--> ligne \(#line): There are \(naturalCount) \(countedThings).")

//: ## 3.2.2.Switch avec la clause 'where'
let pointage = 100000
vies = 0
switch vies {
    case 0 where pointage > 10000: print("--> ligne \(#line): Grace à ton pointage, tu n'es plus mort!"); vies = 1
    case 0:     print("--> ligne \(#line): Té mort!")
    case 1,2:   print("--> ligne \(#line): Attention, la fin est proche...!")
    case 3:     print("--> ligne \(#line): Quelle forme tu as!")
    default:    print("--> ligne \(#line): Tu as certainement triché!")
}

//: ## 3.3.Vérifier la disponibilité d'une API
if #available(iOS 11, macOS 10.12, *) {
    print("--> ligne \(#line): Nous utiliserons un code optimisé pour iOS 11!")
} else {
    print("--> ligne \(#line): Nous utiliserons un code de l'époque de nos parents!")
}

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ControlFlow.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.21 par Alain Boudreault
