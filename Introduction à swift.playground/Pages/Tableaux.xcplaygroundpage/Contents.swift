//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation; import UIKit
//: # Tableaux et dictionnaires

//: # 1.Déclaration

//: ## 1.1.Déclaration implicite (inférence)
let jeSuisUnTableauDeChaines = ["Tintin", "Milou", "Tournesol", "Haddock"]

//: ## 1.2.Déclaration explicite
var moiAussiJeSuisUnTableauDeChaines:Array<String>
moiAussiJeSuisUnTableauDeChaines = jeSuisUnTableauDeChaines

//: ## 1.3.Déclaration explicite courte
var personnages:[String] = jeSuisUnTableauDeChaines
var tableauVideDesChaines:[String] = []


//: # 2.Utilisation

//: ## 2.1.Obtenir un élément
let unPersonnage = personnages[2]
let permierPerso = personnages.first
let dernierPerso = personnages.last

//: ## 2.2.Itérer sur les éléments d'un tableau
for personnage in personnages {
    print("--> ligne \(#line): je suis un personnage de la BD Tintin et mon nom est \(personnage)")
}

//: ## 2.3.Chercher un item dans un tableau
let personnageRecherché = "Milou"
for personnage in personnages where personnage == personnageRecherché {
    print("--> ligne \(#line): Nous avons trouvé \(personnageRecherché) dans le tableau des personnages")
}

//: ## 2.4.Tester s'il y a des éléments dans un tableau
if !personnages.isEmpty {
    print("--> ligne \(#line): Il y a \(personnages.count) personnage(s) dans le tableau")
}


//: # 3.Manipulation

//: ## 3.1.Ajouter un item dans un tableau
personnages.append("Dupond")

//: ## 3.2.Retirer un item du tableau
let retrait = personnages.remove(at:1)

//: ## 3.3.Insérer un item dans un tableau
personnages.insert("Malice", at: 1)

// ## 3.4.Remplacer un (1) ou des items du tableau
// Dans l'exemple suivant: remplacer items 0 à 1 par ...
personnages[0...1] = ["Tuntun", "MunLun", "UnDun", "SunSun"] // ou 0..<2
personnages

//: ## 3.5.Trier un tableau
personnages.sort {a, b in a < b} // voir 'closure'

personnages

//: ## 3.6.Vider un tableau
personnages.removeAll(keepingCapacity: true)

//: ## 3.7.Créer un tableau à plusieurs dimensions
var tableau2D: [[Int]] = [ [1, 2], [3, 4], [5, 6], [7, 8] ]
let troixiemeTableauElement1 = tableau2D[2][0]

var tableau3D: [[[Int]]] = [  [[1, 2], [3, 4]] ,  [[5, 6], [7, 8]] ]
let deuxiemeTableautableau2Element2 = tableau3D[1][1][1]

// Afficher tous les éléments de tableau3D
print("\n\n--> ligne \(#line): Éléments de niveau1")
for (index, niveau1) in tableau3D.enumerated() {
print("  Éléments de niveau1[\(index)]")
    for (index, niveau2) in niveau1.enumerated() {
        print("   N2.[\(index)]")
        for (index, élément) in niveau2.enumerated() {
         print("    Élément[\(index)]: \(élément)")
        }
    }
}

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.18 par Alain Boudreault
