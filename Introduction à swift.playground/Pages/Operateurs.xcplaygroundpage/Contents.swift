//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
//: # Opérateurs
/*:
    Arithmetic Operators

    Swift supports the four standard arithmetic operators for all number types:

    Addition (+)
    Subtraction (-)
    Multiplication (*)
    Division (/)
 */
1 + 2                   // equals 3
5 - 3                   // equals 2
2 * 3                   // equals 6
10.0 / 2.5              // equals 4.0
"hello, " + "world"     // equals "hello, world"
//:---

/*:
    Remainder Operator
 
    The remainder operator (a % b) works out how many multiples of b will fit inside a and returns the value that is left over (known as the remainder).
 */
9 % 4                   // equals 1

//:---
/*:
    Unary Minus Operator

    The sign of a numeric value can be toggled using a prefixed -, known as the unary minus operator:
*/
let three = 3
let minusThree = -three       // minusThree equals -3
let plusThree = -minusThree   // plusThree equals 3, or "minus minus three"

/*:
    The unary minus operator (-) is prepended directly before the value it operates on, without any white space.

    Unary Plus Operator

    The unary plus operator (+) simply returns the value it operates on, without any change:
*/
let minusSix = -6
let alsoMinusSix = +minusSix  // alsoMinusSix equals -6

//: Although the unary plus operator doesn’t actually do anything, you can use it to provide symmetry in your code for positive numbers when also using the unary minus operator for negative numbers.
    
//: ## Compound Assignment Operators
//: Like C, Swift provides compound assignment operators that combine assignment (=) with another operation. One example is the addition assignment operator (+=):
var a = 1
a += 2          // a is now equal to 3
//: The expression a += 2 is shorthand for a = a + 2. Effectively, the addition and the assignment are combined into one operator that performs both tasks at the same time.

//: NOTE: The compound assignment operators do not return a value. For example, you cannot write let b = a += 2.

//: ## Ternary Conditional Operator

/*:
    The ternary conditional operator is a special operator with three parts, which takes the form question ? answer1 : answer2. It is a shortcut for evaluating one of two expressions based on whether question is true or false. If question is true, it evaluates answer1 and returns its value; otherwise, it evaluates answer2 and returns its value.

    The ternary conditional operator is shorthand for the code below:

    if question {
        answer1
    } else {
        answer2
    }
 
    Here’s an example, which calculates the height for a table row. The row height should be 50 points taller than the content height if the row has a header, and 20 points taller if the row doesn’t have a header:
*/
let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20)
// rowHeight is equal to 90


//: ## Nil-Coalescing Operator
/*:
    The nil-coalescing operator (a ?? b) unwraps an optional a if it contains a value, or returns a default value b if a is nil. The expression a is always of an optional type. The expression b must match the type that is stored inside a.

    The nil-coalescing operator is shorthand for the code below:

    a != nil ? a! : b

    The code above uses the ternary conditional operator and forced unwrapping (a!) to access the value wrapped inside a when a is not nil, and to return b otherwise. The nil-coalescing operator provides a more elegant way to encapsulate this conditional checking and unwrapping in a concise and readable form.

    NOTE: If the value of a is non-nil, the value of b is not evaluated. This is known as short-circuit evaluation.

    The example below uses the nil-coalescing operator to choose between a default color name and an optional user-defined color name:
*/
let defaultColorName = "red"
var userDefinedColorName: String?   // defaults to nil

var colorNameToUse = userDefinedColorName ?? defaultColorName
// userDefinedColorName is nil, so colorNameToUse is set to the default of "red"
/*:
    The userDefinedColorName variable is defined as an optional String, with a default value of nil. Because userDefinedColorName is of an optional type, you can use the nil-coalescing operator to consider its value. In the example above, the operator is used to determine an initial value for a String variable called colorNameToUse. Because userDefinedColorName is nil, the expression userDefinedColorName ?? defaultColorName returns the value of defaultColorName, or "red".

    If you assign a non-nil value to userDefinedColorName and perform the nil-coalescing operator check again, the value wrapped inside userDefinedColorName is used instead of the default:
*/
userDefinedColorName = "green"
colorNameToUse = userDefinedColorName ?? defaultColorName
// userDefinedColorName is not nil, so colorNameToUse is set to "green"

//: ## Logical Operators

/*:
    Logical operators modify or combine the Boolean logic values true and false. Swift supports the three standard logical operators found in C-based languages:

    Logical NOT (!a)
    Logical AND (a && b)
    Logical OR (a || b)
    Logical NOT Operator

    The logical NOT operator (!a) inverts a Boolean value so that true becomes false, and false becomes true.

    The logical NOT operator is a prefix operator, and appears immediately before the value it operates on, without any white space. It can be read as “not a”, as seen in the following example:
*/
let allowedEntry = false
if !allowedEntry {
    print("ACCESS DENIED")
}
// Prints "ACCESS DENIED"
/*:
    The phrase if !allowedEntry can be read as “if not allowed entry.” The subsequent line is only executed if “not allowed entry” is true; that is, if allowedEntry is false.

    As in this example, careful choice of Boolean constant and variable names can help to keep code readable and concise, while avoiding double negatives or confusing logic statements.
*/
//: ## Logical AND Operator
/*:
    The logical AND operator (a && b) creates logical expressions where both values must be true for the overall expression to also be true.

    If either value is false, the overall expression will also be false. In fact, if the first value is false, the second value won’t even be evaluated, because it can’t possibly make the overall expression equate to true. This is known as short-circuit evaluation.

    This example considers two Bool values and only allows access if both values are true:
*/
let enteredDoorCode = true
let passedRetinaScan = false
if enteredDoorCode && passedRetinaScan {
    print("Welcome!")
} else {
    print("ACCESS DENIED")
}
// Prints "ACCESS DENIED"
//: ## Logical OR Operator
/*:
    The logical OR operator (a || b) is an infix operator made from two adjacent pipe characters. You use it to create logical expressions in which only one of the two values has to be true for the overall expression to be true.

    Like the Logical AND operator above, the Logical OR operator uses short-circuit evaluation to consider its expressions. If the left side of a Logical OR expression is true, the right side is not evaluated, because it cannot change the outcome of the overall expression.

    In the example below, the first Bool value (hasDoorKey) is false, but the second value (knowsOverridePassword) is true. Because one value is true, the overall expression also evaluates to true, and access is allowed:
*/
let hasDoorKey = false
let knowsOverridePassword = true
if hasDoorKey || knowsOverridePassword {
    print("Welcome!")
} else {
    print("ACCESS DENIED")
}
// Prints "Welcome!"
//: ## Combining Logical Operators
//: You can combine multiple logical operators to create longer compound expressions:
    
    if enteredDoorCode && passedRetinaScan || hasDoorKey || knowsOverridePassword {
    print("Welcome!")
} else {
    print("ACCESS DENIED")
}
// Prints "Welcome!"
/*:
    This example uses multiple && and || operators to create a longer compound expression. However, the && and || operators still operate on only two values, so this is actually three smaller expressions chained together. The example can be read as:

    If we’ve entered the correct door code and passed the retina scan, or if we have a valid door key, or if we know the emergency override password, then allow access.

    Based on the values of enteredDoorCode, passedRetinaScan, and hasDoorKey, the first two subexpressions are false. However, the emergency override password is known, so the overall compound expression still evaluates to true.

    NOTE: The Swift logical operators && and || are left-associative, meaning that compound expressions with multiple logical operators evaluate the leftmost subexpression first.
*/
//: ## Explicit Parentheses
/*:
    It is sometimes useful to include parentheses when they are not strictly needed, to make the intention of a complex expression easier to read. In the door access example above, it is useful to add parentheses around the first part of the compound expression to make its intent explicit:
*/
if (enteredDoorCode && passedRetinaScan) || hasDoorKey || knowsOverridePassword {
    print("Welcome!")
} else {
    print("ACCESS DENIED")
}
// Prints "Welcome!"
/*:
    The parentheses make it clear that the first two values are considered as part of a separate possible state in the overall logic. The output of the compound expression doesn’t change, but the overall intention is clearer to the reader. Readability is always preferred over brevity; use parentheses where they help to make your intentions clear.
*/

//:---
//: # 2 - Opérateurs avancés
//: Voir: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/AdvancedOperators.html#//apple_ref/doc/uid/TP40014097-CH27-ID28
//: Helper function
//----------------------------------------------
func afficherBinaire(_ unNombre:UInt8) -> String {
    var padded = String(unNombre, radix: 2)
    for _ in 0..<8 - padded.characters.count {
        padded = "0" + padded
    }
    return padded
} // afficherBinaire
//----------------------------------------------

//:  Opération sur les bits
typealias Octet = UInt8
var unNombre:Octet = 0b0000001
afficherBinaire(unNombre)
//: Tasser les bits vers la gauche
unNombre << 1
unNombre << 2
var x = unNombre << 3
afficherBinaire(x)
//: bitWise OR
afficherBinaire(x | 0b00000001)
//: bitWise AND
afficherBinaire(x & 0b00000111)
//: bitWise XOR
afficherBinaire(~x)

//: Tasser les bits vers la droite
afficherBinaire(x >> 1)


//: Operator Methods
/*:
    Classes and structures can provide their own implementations of existing operators. This is known as overloading the existing operators.

    The example below shows how to implement the arithmetic addition operator (+) for a custom structure. The arithmetic addition operator is a binary operator because it operates on two targets and is said to be infix because it appears in between those two targets.

    The example defines a Vector2D structure for a two-dimensional position vector (x, y), followed by a definition of an operator method to add together instances of the Vector2D structure:
*/
struct Vector2D {
    var x = 0.0, y = 0.0
}

extension Vector2D {
    static func + (left: Vector2D, right: Vector2D) -> Vector2D {
        return Vector2D(x: left.x + right.x, y: left.y + right.y)
    }
}
/*:
    The operator method is defined as a type method on Vector2D, with a method name that matches the operator to be overloaded (+). Because addition isn’t part of the essential behavior for a vector, the type method is defined in an extension of Vector2D rather than in the main structure declaration of Vector2D. Because the arithmetic addition operator is a binary operator, this operator method takes two input parameters of type Vector2D and returns a single output value, also of type Vector2D.

    In this implementation, the input parameters are named left and right to represent the Vector2D instances that will be on the left side and right side of the + operator. The method returns a new Vector2D instance, whose x and y properties are initialized with the sum of the x and y properties from the two Vector2D instances that are added together.

    The type method can be used as an infix operator between existing Vector2D instances:
*/
let vector = Vector2D(x: 3.0, y: 1.0)
let anotherVector = Vector2D(x: 2.0, y: 4.0)
let combinedVector = vector + anotherVector
// combinedVector is a Vector2D instance with values of (5.0, 5.0)

//: [Référence - Opérateurs de base](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/BasicOperators.html)
//: [Référence - Opérateurs avancés](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/AdvancedOperators.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
