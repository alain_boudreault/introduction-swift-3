//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
//: # Énumérations et optionSets

//: # 1.Énumérations
// Déclaration
enum TypeDeJoueur  {
    case novice
    case inter
    case expert
} // enum TypeDeJoueur

// Utilisation
let joueur01:TypeDeJoueur = .novice

if joueur01 != .expert {
    print("Le joueur n'est pas un expert")
} // if

switch joueur01 {
case .novice: print("Le joueur est de niveau novice.")
case .inter:  print("Le joueur est de niveau inter.")
case .expert: print("Le joueur est de niveau expert.")
    // Note, pas besoin de clause 'default:' car tous les choix sont épuisés.
} // switch

//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Enumerations.html)

//:------
//: # 2.OptionSets
struct EquipementDeGuerrier : OptionSet {
    let rawValue: Int
    static let casque       = EquipementDeGuerrier(rawValue: 1 << 0) // 00000001  1
    static let épée         = EquipementDeGuerrier(rawValue: 1 << 1) // 00000010  2
    static let bottes       = EquipementDeGuerrier(rawValue: 1 << 2) // 00000101  4
    static let armure       = EquipementDeGuerrier(rawValue: 1 << 3) // 00001000  8
    static let anneau       = EquipementDeGuerrier(rawValue: 1 << 4) // 00010000  16
    static let cape         = EquipementDeGuerrier(rawValue: 1 << 5) // 00100000  32
    static let gantelets    = EquipementDeGuerrier(rawValue: 1 << 6) // 01000000  64
    static let jambières    = EquipementDeGuerrier(rawValue: 1 << 7) // 10000000  128
    
    static let toutÉquipé:EquipementDeGuerrier = [.casque, .épée, .bottes, .armure, .anneau, .cape, .gantelets, .jambières]
    static let équipementDeBase:EquipementDeGuerrier = [.bottes, .cape]
}
// ce qui va donner:
// Créer une variable de type OptionSet
var monÉquipement:EquipementDeGuerrier = [.armure, .bottes, .casque] // 00001011 = 13

// Ajouter une option
monÉquipement.insert(.jambières)
print(monÉquipement)

var msg = monÉquipement.contains(.casque) ? "J'ai":"Je n'ai pas"
print ("\(msg) le casque!")

// Retirer une option
monÉquipement.subtract(.casque)
msg = monÉquipement.contains(.casque) ? "J'ai":"Je n'ai pas"
print ("\(msg) le casque!")

//: [Référence](https://developer.apple.com/reference/swift/optionset)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.20 par Alain Boudreault