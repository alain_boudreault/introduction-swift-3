//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
import UIKit

//: # Les protocoles

//: # 1.Protocoles de classes

//TODO:

protocol PoissonDelegate {
    func poissonEstMort(sender:Poisson)
}

class Poisson{
    var delegate:PoissonDelegate?
    var nom:String
    var force:Int
    
    init(_ nom:String, force:Int) {
        self.nom = nom
        self.force = force
    } // init()
    
    func affaiblir(force:Int) {
        self.force -= force
        if self.force <= 0{
            delegate?.poissonEstMort(sender: self)
        }
    } // affaiblir
} // class Poisson

// #############################################
class Application:PoissonDelegate {
    var unPoisson:Poisson
    
    // ################
    init(_ nom:String) {
        self.unPoisson = Poisson(nom, force: 2)
        unPoisson.delegate = self
        unPoisson.affaiblir(force: 4)
    } // Fin du constructeur
    
    // ################
    func poissonEstMort(sender: Poisson) {
        print("Le poisson est mort!")
    }
} // class uneApplication
// #############################################

let uneApplication = Application("Toto")



//: ############################################################
//: # 2.Protocoles de Swift
protocol Rectangle: Equatable {
    var width: Double { get }
    var height: Double { get}
}

// all objects that conform to this protocol
// will now have default equality based on the
// protocol properties
func ==<T: Rectangle>(lhs: T, rhs: T) -> Bool {
    return lhs.width == rhs.width &&
        rhs.height == lhs.height
}

struct ColorfulRectangle: Rectangle {
    let width: Double
    let height: Double
    let color: UIColor
}

let rect1 = ColorfulRectangle(width: 10, height: 10, color: .blue)
let rect2 = ColorfulRectangle(width: 10, height: 10, color: .red)

// these two rectangles are the same,
// despite having a different color
rect1 == rect2 // true

func ==(lhs: ColorfulRectangle, rhs: ColorfulRectangle) -> Bool {
    return lhs.width == rhs.width &&
        rhs.height == lhs.height &&
        rhs.color == lhs.color
}


// ======================================
// Égalité sur personnage

enum typePersonnage{
    case Boss
    case suiveux
}

// Déclaration du protocole
protocol PersonnageEquatable: Equatable {
    // var force:Int // non valide dans un protocole.  Il faut utiliser get/set
    var force:Int { get }
    var type:typePersonnage { get }
    
} // protocol PersonnageEquatable

// Implémentation du protocole "PersonnageBase:Equatable"
// Note: <T: PersonnageEquatable> = déclaration d'un type générique 'T'
func ==<T: PersonnageEquatable>(p1: T, p2: T) -> Bool {
    return p1.force == p2.force &&
        p1.type == p2.type
}

class Personnage:PersonnageEquatable {
    var force:Int
    var type:typePersonnage
    var nom:String

    init(nom:String, force:Int, type:typePersonnage) {
        self.nom = nom
        self.type = type
        self.force = force
    }
}

var p1 = Personnage(nom: "Yo", force: 99, type: .Boss)
var p2 = Personnage(nom: "Mo", force: 99, type: .Boss)

p1 == p2

class PersonnageDeNiveau2:Personnage {
    // Cette classe sera "Equatable"
}

// Hashable
// <-- effacer: let personnages:Dictionary<Personnage, String>

/* <-- effacer:
// https://fr.wikipedia.org/wiki/Fonction_de_hachage 
extension Personnage: Hashable {
    var hashValue:Int {
        return self.nom.hashValue
    }
}

let personnages:Dictionary<Personnage, String> =
    [p1:"je suis le personnage P1",
     p2:"je suis le personnage P1"
]

print("\(#line) -", personnages[p1]!)

*/


//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.01.09 par Alain Boudreault

