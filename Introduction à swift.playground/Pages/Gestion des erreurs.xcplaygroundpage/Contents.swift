//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
import UIKit
//: # Gestion des erreurs


// Déclaration des actions (optionnelle)
enum Action {
    case succès, succèsMitigé, échec
} // enum Action

// 1 - Déclaration des erreurs
enum ErreursDelaFonctionDangereuse: Error {
    case pasTropGrave
    case IlVaSurvivre
    case yéMort
} // enum ErreursDelaFonctionDangereuse

// 2 - Déclarer un fonction qui 'produit/throws' des erreurs
func uneFonctionDangereuse(action:Action) throws -> String
{
    switch action {
    case .succès:           return "Bravo!"
    case .succèsMitigé:     throw ErreursDelaFonctionDangereuse.IlVaSurvivre
    case .échec:            throw ErreursDelaFonctionDangereuse.yéMort
    } // switch action
 } // uneFonctionDangereuse

// 3 - Tester la fonction qui produit des erreurs
// Sans discrimination des erreurs:
let _action = Action.succès
do {
    let résultat = try uneFonctionDangereuse(action: _action)
    print("--> ligne \(#line): Traitement réussi, résultat = \(résultat)")
}
catch {
    print("--> ligne \(#line): Erreur: quelconque")
}

// En précisant une erreur:
do {
    let résultat = try uneFonctionDangereuse(action: _action)
}
catch ErreursDelaFonctionDangereuse.IlVaSurvivre {
    print("--> ligne \(#line): Erreur: de moindre conséquence...")
}
catch ErreursDelaFonctionDangereuse.yéMort {
    defer { print("Ménage effectué!")}  // Exécuté un fois sortie du bloc.
    print("--> ligne \(#line): Erreur: très grave!")

}

//: TODO: instruction 'guard'
//guard password.characters.count > 0 else { throw EncryptionError.Empty }
//guard password.characters.count >= 5 else { throw EncryptionError.Short }
enum typePersonnage{
    case Boss
    case suiveux
}

class Personnage{
    var nom:String
    var force:Int
    var type:typePersonnage
    var position = CGPoint(x: 0, y: 0)

    init(nom:String, type:typePersonnage, force:Int) {
        self.nom    = nom
        self.type   = type
        self.force  = force
    }
} // struct Personnage

extension Personnage:CustomStringConvertible {
    public var description:String {
        return "Je suis le personnage \(self.nom) de type \(self.type)"
    }
}

// 1 - Déclaration des erreurs
enum ErreursDuPersonnage: Error {
    case pasUnPersonnage
    case pasUnBoss
    case pasDeForce
    case yéMort
} // enum ErreursDelaFonctionDangereuse

func déplacer(_ yo:Any) throws {
    guard yo is Personnage else { throw ErreursDuPersonnage.pasUnPersonnage}
    print("Nous avons déplacer le personnage...)")
}

let perso1 = Personnage(nom: "toto", type: .Boss, force: 99)

// Tester la classe "Personnage"
do {
    try déplacer(perso1)
    try déplacer("yo")
    print("\(#line), Cette ligne ne sera pas exécutée...")
}
    catch ErreursDuPersonnage.pasUnPersonnage {
    print("--> Erreur: l'objet n'est pas de type 'Personnage'")
}

// Tester le type du personnage.
// Il est possible d'ajouter des suiveux que sur un personnage de type .Boss
func ajouterSuiveux(_ yo:Personnage, suiveux:[String]) throws {
    guard yo.type == .Boss else {
        throw ErreursDuPersonnage.pasUnBoss
    }
    print("Les suiveux suivants ont été ajoutés à \(yo.nom): \(suiveux)")
}

let perso2 = Personnage(nom: "Bob", type: .suiveux, force: 99)
do {
    try ajouterSuiveux(perso1, suiveux: ["pre", "prou"])
    try ajouterSuiveux(perso2, suiveux: ["Boule", "Bill"])
    
} catch ErreursDuPersonnage.pasUnBoss {
    print("--> Erreur: Le personnage n'est pas de type 'Boss'")
}

// Voir la doc d'Apple pour plus d'info:
// https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)


//: -------------------------------------------
//: Révision du 2017.01.05 par Alain Boudreault

