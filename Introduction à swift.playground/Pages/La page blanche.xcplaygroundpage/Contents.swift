//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation; import UIKit
// Votre code ici:

var unLabel:UILabel

unLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
unLabel.backgroundColor = UIColor.lightGray
unLabel.text = "Je suis du texte..."

var unAutreLabel:UILabel?

unAutreLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
unAutreLabel?.backgroundColor = UIColor.lightGray
unAutreLabel?.textColor = UIColor.white
unAutreLabel!.text = "Je suis encore du texte..." // BOUM!
//unAutreLabel?.text = nil
//unAutreLabel?.text!.uppercased()  // BOUM!
unAutreLabel?.text?.uppercased()


let texte = String(format: "%2.2f", 3.141592)

