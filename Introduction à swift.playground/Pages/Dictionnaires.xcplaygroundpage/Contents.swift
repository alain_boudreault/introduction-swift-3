//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation; import UIKit
//: # 1.Les dictionnaires

//: ## 1.1.Déclaration implicite de type <String, String>
var personnage01 = [ "Nom": "Fred", "Force": "99", "Rang":"Novice"]

//: ## 1.2.Déclaration implicite de type <String, AnyObject>
// var personnage01_2 = [ "Nom": "Fred", "Force": 99, "Rang":"Novice"]  // pas possible!


//: ## 1.3.Déclaration explicite
let personnage02:Dictionary<String, String> = [ "Nom": "Tom", "Force": "34", "Rang":"Expert"]
// Ceci est équivalent:
let unDictionnaire: [String: Int] = ["Fred": 31, "Lise": 39]
// ou bien
let encoreUnDictionnaire = ["Fred": 31, "Lise": 39] as [String: Int]


//: ## 1.4.Déclaration explicite de type Any
let personnage03:Dictionary<String, Any> = [ "Nom": "Tom", "Force": 34, "Rang":"Expert", "couleur": UIColor.red]

//: ## 1.5.Déclaration d'un dictionnaire vide
var unDictionnaireVide      = Dictionary<String, String>()
var unAutreDictionnaireVide = Dictionary<String, Any>()

//: # 2.Utilisation d'un dictionnaire

//: ## 2.1.Utilisation d'un dictionnaire de type <String,String>
let nomPerso1 = personnage01["Nom"]  // va créer une optionnelle!
// Note: Il est recommandé d'utiliser la syntaxe 'if set'. Un champ non défini retournera 'nil'
var unNom = ""
// unNom = nomPerso1                 // va produire une erreur à moins de déballer 'nomPerso1!'

let champforce = "Force"
if let forcePerso1 = personnage01[champforce] {
    print("--> ligne \(#line): La \(champforce) de Perso1 = \(forcePerso1)")
} else {
    print("--> ligne \(#line): Erreur: Impossible d'obtenir la \(champforce) du personnage...")
}

//: ## 2.2.Utilisation d'un dictionnaire avec des éléments de type 'Any'
let force   = personnage03["Force"] as? Int     // Va créer une optionnelle
let nom     = personnage03["Nom"]   as! String  // Va créer une variable non optionnelle ou peut-être planter.
// Attention, pour plus de robustesse, il faudrait utiliser des liaisons optionnelles.
if let force = personnage03["Force"] as? Int, let nom  = personnage03["Nom"] as? String {
    print("--> ligne \(#line): Mon nom est: \(nom) et j'ai une force de: \(force)")
}

//: ## 2.3.Un tableau de dictionnaires
// Créer des dictionnaires:
let personnage001 = [ "Nom": "Le petit", "Force": "3", "Rang":"novice"]
let personnage002:Dictionary<String, Any> = [ "Nom": "Tom", "Force": 34, "Rang":"Expert"]
// Créer un tableau de dictionnaires
var tableauDePersonnages:Array<Dictionary<String, Any>> = Array()
// Ou bien,
var tableauDesVideos = Array<Dictionary<String,String>>()


// Ajouter des dictionnaires au tableauDesPersonnages:
tableauDePersonnages.append(personnage001)
tableauDePersonnages.append(personnage002)

// Obtenir des informations à partir du tableau:
tableauDePersonnages[0]["Rang"]
tableauDePersonnages[1]["Force"]

//let total = 22 + personnages02[1]["Force"]  // erreur!
// Solution
if let fp = tableauDePersonnages[1]["Force"] as? Int {
    let i = fp + 22
    i
} else
{
    print("--> ligne \(#line): Erreur sur le type du champ 'force'")
}


//: ## 2.4.Un tableau d'AnyObject
let personnage0001 = [ "Nom": "Le petit", "Force": 3,  "Rang":"novice", "Amis": ["Moi","Toi","Lui"]] as [String : Any]
let personnage0002 = [ "Nom": "Le grand", "Force": 78, "Rang":"Expert", "Amis": ["Eux","Nous","Ils"]] as [String : Any]
let personnage0003:Dictionary<String, Any> = [ "Nom": "Le Boss",  "Force": 99, "Rang":"Expert", "Amis": ["Monstre rouge"]]
let personnage0004 = UIColor.blue  //Noter le type!

var unTableauDePersonnages3:Array<AnyObject> = Array()

unTableauDePersonnages3.append(personnage0001 as AnyObject)
unTableauDePersonnages3.append(personnage0002 as AnyObject)
unTableauDePersonnages3.append(personnage0003 as AnyObject)
unTableauDePersonnages3.append(personnage0004 as AnyObject)

print("\n--> ligne \(#line): \(unTableauDePersonnages3)")
unTableauDePersonnages3.count

let nom3 = unTableauDePersonnages3[1]["Nom"] as! String  //Danger ici!

// Utilisation d'une structure NSObject
let nbAmisPerso3 = (unTableauDePersonnages3[2]["Amis"] as! NSArray).count  // Pas besoin de 'caster' le type
let nbAmisPerso3b = (unTableauDePersonnages3[2]["Amis"] as! Array<String>).count

if let amisDuPersonnage2 = unTableauDePersonnages3[1]["Amis"] as? Array<String> {
    print ("\n--> ligne \(#line): nb amisDuPersonnage2: \(amisDuPersonnage2.count), contenu: \(amisDuPersonnage2.count) ")
}

// Afficher les amis des personnages:
for perso in unTableauDePersonnages3 {
    if let nomPerso = perso["Nom"] as? String {  // Note: Il n'y aura pas d'itération pour 'personnage0004', pas de 'clé' 'nom'.
        print("\n--> ligne \(#line): Amis de \(nomPerso): ")
        for ami in (perso["Amis"] as? Array<String>)! {
            print("     --> ligne \(#line): \(ami)")
        }
        print("--------------------------------------")
    }
}

//: ## 2.5.Insérer un nouveau champ - seulement avec <String,String>
personnage01["Vies"] = "4"
personnage01

//: ## 2.6.Retirer un champ - seulement avec <String,String>
personnage01["Rang"] = "Level 1"            // Ajouter le champ 'Rang'
personnage01
personnage01.removeValue(forKey: "Rang")    // Supprimer le champ 'Rang'
personnage01

//: ## 2.7 NSSet – Tableau sans doublon
let personnages4 = ["Tintin", "Milou", "Tournesol", "Haddock", "Allan", "Rastapopoulos", "Dupont", "Dupond", "Muller", "Lampion", "Tapioca" ]

print("\n\n--> ligne \(#line)")
let unEnsemble = NSMutableSet()  // Un tableau sans doublon
var nbIterations = 0
while unEnsemble.count <= 7 {
    let unNombreAuHasard = arc4random_uniform(UInt32(personnages4.count))
    let unPersonnage = personnages4[Int(unNombreAuHasard)]
    print(unPersonnage)
    unEnsemble.add(unPersonnage)
    nbIterations += 1
} // while unEnsemble.count <= 7

print("\n\n--> ligne \(#line): Itérations: \(nbIterations), contenu de l'ensemble: \(unEnsemble)")

//: ## 2.8.NSCountedSet – calcule des doublons
print("\n\n--> ligne \(#line)")
let personnages5 = ["Tintin", "Milou", "Tournesol", "Haddock", "Allan", "Rastapopoulos", "Dupont", "Dupond", "Muller", "Lampion", "Tapioca" ]
let unEnsemble2 = NSCountedSet()  // Un tableau sans doublon
var nbIterations2 = 0
while unEnsemble2.count <= 10 {
    let unNombreAuHasard = arc4random_uniform(UInt32(personnages5.count))
    let unPersonnage = personnages5[Int(unNombreAuHasard)]
    print(unPersonnage)
    unEnsemble2.add(unPersonnage)
    nbIterations += 1
}

print("\n\n\(unEnsemble2)")
for x in unEnsemble2 {
    print("--> ligne \(#line): Personnage: \(x), nb d'insertions: \(unEnsemble2.count(for: x))")
}


//: ## 2.9.NSIndexSet – Un ensemble d’indices

let personnages = ["Tintin", "Milou", "Tournesol", "Haddock", "Allan", "Rastapopoulos", "Dupont", "Dupond", "Muller", "Lampion", "Tapioca" ]

let indexSet = NSMutableIndexSet()  // Todo: remplacer par IndexSet()
indexSet.add(0)
indexSet.add(2)
indexSet.add(4)

(personnages as NSArray).objects(at: indexSet as IndexSet) // retourne; Tintin, Tournesol et Allan

//: #3.Créer un tableau à partir d'une source externe

//: ## 3.1.Créer un tableau de dictionnaires à partir d'un fichier: NSArray(contentsOfFile:)
// Voir: http://rebeloper.com/read-write-plist-file-swift/
// Chargement de la liste des vidéos
if let path = Bundle.main.path(forResource: "Listes des videos", ofType: "plist")
{
    if let tableauDesVideos = NSArray(contentsOfFile: path) as? Array<Dictionary<String, String>> {
        for video in tableauDesVideos {
            if let titre = video["titre"] {
                print("--> ligne \(#line): Le titre du film est '\(titre)'")
            } // if let titre
        } // for video in
    } // if let tableauDesVideos
} // if let path
else {
    print("--> ligne \(#line): Erreur: Le chemin vers le fichier de données est invalide ")
} // if let path ...

//: ## 3.2.Créer un tableau de dictionnaires à partir d'une URL: NSArray()
var lesAmisDeLaScienceData = Array<Dictionary<String, String>>()
let strURL = "http://prof-tim.cstj.qc.ca/cours/xcode/sources/amisDelaScience.plist"
if let uneURL = URL(string: strURL) {
    if let _données = NSArray(contentsOf: uneURL) as? Array<Dictionary<String, String>> {
        lesAmisDeLaScienceData = _données
        print(_données)
    } else
    {
        print("\n\n#Erreur: impossible de créer un tableau à partir du fichier... \(strURL)\n\n")
    } // Problème avec l'URL
}  else {
    print("\n\n#Erreur: impossible de lire les données via \(strURL)\n\n")
}
//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2016.09.18 par Alain Boudreault
