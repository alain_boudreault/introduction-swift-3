//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import UIKit
//: # Variables et optionnelles

//: # 1.Variables

//: ## 1.1.Types de base
// TYPE	                       EXEMPLE
// Int – UInt – UInt32         0 ; 1 ; 55 ; 32767 ; -2
// Float – CGFloat – Double    2.345; 3.141592 ; -105678.999
// Bool                        true ; false
// String                      'Paul' ; "CSTJ.qc.ca"
// ClassName                   UIButton, UIColor, UIImage, Personnage
// Any, AnyObject              uneFonction(sender:AnyObject)
//: Note: **AnyObject** pour les classes des frameworks et **Any** pour les classes des frameWorks et les types de swift.

//:  ## 1.1.1.Valeurs Limites pour les entiers
//:  Entiers non signés:
UInt8.min
UInt8.max
UInt16.min
UInt16.max
UInt32.min
UInt32.max
UInt64.min
UInt64.max
UInt.min
UInt.max

//: Entiers signés:
Int8.min
Int8.max
Int16.min
Int16.max
Int32.min
Int32.max
Int64.min
Int64.max
Int.min
Int.max
//:---
//: ## 1.1.2.Nombres à virgule flotante (Floating-Point Numbers)
/*:
 Les nombres à virgule flotante sevent à représenter des nombres avec fraction, tel que:  3.14159, 0.1, et -273.15.

 swift propose deux types de nombres à virgule flotante:

 **'Double'** pour représenter un nombre à virgule flotante sur 64-bits (précision d'au moins 16 décimales).
 
 **'Float'** pour représenter un nombre à virgule flotante sur 32-bits (précision d'au moins 6 décimales).
*/
//:---
//: ## 1.2.Déclaration
//: ## 1.2.1.Déclaration explicite
var unEntier:Int        = 123 ;
var unOctet:UInt8       = 0;
var unRéel:Float        = 3.1415
var unDouble:Double     = 3.1415920123456789087654321
var unBooléen:Bool      = true
var uneChaine:String    = "CSTJ.TIM"
var uneVue:UIView       = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
uneVue.backgroundColor  = UIColor.purple

//: ## 1.3.Déclaration implicite (le type sera déduit, inféré, par Xcode)
var unEntierTypeInféré  = 99
var unNombreBinaire     = 0b0000011  // Notation binaire
var unNombreHex         = 0xff       // Notation hexadécimale
var unNombreOct         = 0o10       // Notation octale
var unRéelTypeInféré    = 3.1415     // Double
var unDoubleTypeInféré  = 3.1415920123456789087654321
var unBooléenTypeInféré = true
var uneChaineTypeInféré = "CSTJ.TIM"
var unClownMéchant      = UIImage(named: "clownMechant")
//: ## 1.4.Variable versus Constante
var uneVariableMutable  = 2.2    // anglais : mutable
uneVariableMutable      = 1.4241

let uneVariableImmutable = 9.9   // anglais : immutable
// uneVariableImmutable = 123    // erreur de compilation

//: Note:  Apple recommande l’utilisation de constantes à la place de variables lorsque possible.

//: ## 1.5.Variable avec valeur non optionnelle
var uneChaineAvecValeurNonOptionnelle :String
// print(uneChaineAvecValeurNonOptionnelle) // produira une erreur à l’exécution
// uneChaineAvecValeurNonOptionnelle = nil  // produira une erreur à l’exécution
//:---
//: # 2.Les optionnelles
/*: Définition:
 Les optionnelles permettent de déclarer des variables qui auront, ou pas, une valeur.
*/

//: ## 2.1.Déclaration
var c1:String? // = "toto"
var c2:String? // = "titi"
var c3:String? = "Yoyo"
var c4:Optional<String> // Une autre façon de déclarer une optionnelle.

// let y:String = c1    // produira une erreur à l’exécution
// let y:String = c1!   // produira une erreur à l’exécution

//: ## 2.2.Liaison optionnelle (optional binding)
if let d1 = c1 {
    print ("--> ligne \(#line): Liaison réussie de c1=, '\(c1)', c1!:'\(c1!)' et d1:'\(d1)'")
}

if let d1 = c1, let d2 = c2 {
    print ("--> ligne \(#line): Liaison réussie de c1 et c2")
}

if let d1 = c1, let d2 = c2, let d3 = c3 {
    print ("--> ligne \(#line): Liaison réussie de c1, c2,et c3")
} else {
    print("--> ligne \(#line): Meilleur chance la prochaine fois!")
}

if c3 == nil {
    print("Yo!")
} else {
    let yo = c3!
}

//: ## 2.2.1.Tester la nullité d'une variable
let jeSuisTellementNul:Any? = "nil"
let conséquence = jeSuisTellementNul == nil ? "c'est vrai" : "Non, j'ai la valeur: \(jeSuisTellementNul!)"

//: ## 2.2.1.Opérateur de fusion 'Null' (en:null) (Nil-Coalescing Operator)
let avatarParDéfaut = UIImage(named: "avatarParDefaut")
let avatarDuJoueur  = UIImage(named: "avatarDeTto")
let unAvatar = avatarDuJoueur ?? avatarParDéfaut  // ?? = Nil-Coalescing Operator
// Référence: https://en.wikipedia.org/wiki/Null_coalescing_operator

//: ## 2.3.Chaînage Optionnel (optional chaining)
var unLabel:UILabel?

unLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 720, height: 60))
unLabel?.backgroundColor = UIColor.lightGray
unLabel?.textColor = UIColor.white
unLabel?.font?.fontName  // Afficher le nom de la police
unLabel?.font = UIFont(name: "Courier" /* (unLabel?.font?.fontName)! */, size: 50)

unLabel!.text = " Je suis du gros texte!" // BOUM!

unLabel?.text = nil
// unLabel?.text.uppercased()                // BOUM!
unLabel?.text?.uppercased()

//: ## 2.4.Plusieurs constructeurs
let texte = String(format: "%2.2f", 3.141592)

//:---
//: # 3.Conversion

//: ## 3.1.Conversion d'une chaine vers une valeur numérique
let nombreProbable = "123A"
let nombreConverti:Int // = Int(nombreProbable) // Note: Enlever le commentaire, BOUM!
let valeurConvertieOptionnelle:Int? = Int(nombreProbable)
if let nombre = Int(nombreProbable) {
    print("--> ligne \(#line): Conversion réussie, résultat = \(nombre)")
}
//:---
//: # 4.Fonction avec une valeur de retour optionnelle

//: ## 4.1.Fonction avec une valeur de retour
func somme(_ var1:Int, _ var2:Int) -> Int {
    return var1 + var2
} // somme

//: ## 4.2.Fonction avec une valeur de retour optionnelle
func optSomme(_ var1:Int?, _ var2:Int?) -> Int? {
    if let nombre1 = var1, let nombre2 = var2 {
        return nombre1 + nombre2
    }  // if
    return nil
} // optSomme

//: ## 4.3.Utilisation d'une fonction avec valeur de retour
_ = somme(1, 2)

//: ## 4.3.Utilisation d'une fonction avec valeur de retour optionnelle
var i:Int? // = 99
var resultat:Int
// resultat = optSomme(i, 3)    // Note: Enlever le commentaire!

if let resultat = optSomme(i, 3) {
    print("--> ligne \(#line): Nous avons obtenu le résultat suivant: \(resultat)")
}

//guard resultat = optSomme(i, 3) else {} // Voir Gestion des erreurs
//: # 5.typeAlias (type perso)
typealias Octet = UInt8
var monOctet:Octet
monOctet = 0b11111111

//
typealias DictionnaireDeChaines = Dictionary<String, String>
var unDictionnaireDeChaines:DictionnaireDeChaines = [:]

//
typealias DictionnaireDe<Value> = Dictionary<String, Value>
var unDictionnaireDeInt:DictionnaireDe<Int> = [:]
var unDictionnaireDeCouleurs:DictionnaireDe<UIColor> = ["Rouge":UIColor.red, "Vert":UIColor.green]

//: # 6.Setters et getters

//TODO:

class UneClasse {
    private var _i:Int
    internal var i:Int {
        get { return _i }
        set(valeur /* newValue */) { _i = valeur >= 0 ? valeur : 0 }
    }
    
    init(_ i:Int) {
        _i = i
    }
} // class UneClasse

let uneClasse = UneClasse(99)
uneClasse.i
uneClasse.i = -24
uneClasse.i
// Note:  Dans l'exemple précédant, il est préférable d'utiliser 'didSet'. Voir le point #7

// Un autre exemple,
class Température {
    // Note: celsius est une 'stored propertie'
    var celsius: Float = 0.0
    
    // Note: fahrenheit est une 'computed propertie' à partir de la valeur de 'celsius'.  
    // Dans ce cas, nous aurons besoin de 'setter/getter'.
    var fahrenheit: Float {
        get {
            return (celsius * 1.8) + 32.0
        }
        set {
            celsius = (newValue - 32)/1.8
        }
    }
}

// Exemple d'utilisation de la classe 'Température'
var temp = Température()
temp.fahrenheit = 89
temp.celsius


//: # 7.Précurseur et résultant (les observateurs willSet et didSet)
// Note: Property Observers are not called during Initialization
// Encore une note:  une variable sous observation doit avoir une valeur de départ ou être renseignée par une méthode d'initialisation.

var proverbe:String = "Yo" {  // Prend une valeur de départ si pas dans une classe avec un init()

    willSet {
        print("Sur le point de changer le proverbe pour:  \(newValue)")
    } // willSet
    
    didSet {
        if proverbe == oldValue {
            print("L'inertie est la mère de tous les maux.")
            // Alors voici un autre proverbe:
            proverbe = "L'inertie est la mère de tous les maux."
        } // didSet
    }
} // FIN de la déclaration de la variable 'proverbe'

proverbe = "Yo"

print("\(#line): \(proverbe)")

//: # 8.NumberFormatter
let numberFormatter = NumberFormatter()
numberFormatter.numberStyle = .currency
//numberFormatter.locale = Locale.current
print(numberFormatter.string(from: 3.14)!,"\n")

numberFormatter.numberStyle = .spellOut
// https://gist.github.com/jasef/337431c43c3addb2cbd5eb215b376179
for identifier in ["en_US", "fr_FR", "vi_VN", "zh_Hans"] {
    numberFormatter.locale = Locale(identifier: identifier)
    print("\(identifier):\t \(numberFormatter.string(from: 3.141592)!)")
}



/*
// Exemple de code pour afficher tous les identificateurs.
print("localeIdentifier, Description")
let identifiers: NSArray = NSLocale.availableLocaleIdentifiers as NSArray
let locale = NSLocale(localeIdentifier: "en_US")

identifiers.forEach {
    let name = locale.displayName(forKey: NSLocale.Key.identifier, value: $0)!
    print("\($0),\"\(name)\"")
}
*/


//: [Référence](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html)

//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.02.24 par Alain Boudreault
