//: [menu](Menu) - [suivant](@next) - [précédent](@previous)
import Foundation
import UIKit

//: # Ressources externes

// Afficher une image dans la source
// -> //: ![texte alternatif](nomFicherImage texte de survole)
//: ![Toto](avatarDeToto.png "je suis Toto!")

// Utiliser une image dans le Web:
//: ![une image](https://httpbin.org/image/png "")

// Autre façon d'insérer une image, la  glisser dans le code:
let méchant = #imageLiteral(resourceName: "clownMechant.png")


// Charger un fichier de contenu
// fichier.txt -> String

if let cheminVersFichier = Bundle.main.path(forResource: "unFichier", ofType: "txt"),
    let data = FileManager.default.contents(atPath: cheminVersFichier),
    let texte = String(data: data, encoding: .utf8){
        print("Ligne: \(#line), texte: \(texte)")
    }

// Glisser un fichier dans le code:
let cheminFichier = #fileLiteral(resourceName: "unFichier.txt")  // chemin vers la ressource
print("Ligne: \(#line), \(cheminFichier)")

// Utiliser un 'color literal'
let vert = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)


//: [menu](Menu) - [suivant](@next) - [précédent](@previous)

//: -------------------------------------------
//: Révision du 2017.02.21 par Alain Boudreault
